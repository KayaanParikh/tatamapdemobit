//
//  TataMapDemoUITests.swift
//  TataMapDemoUITests
//
//  Created by Kayaan on 04/06/21.
//  Copyright © 2021 Abhilash Mathur. All rights reserved.
//

import XCTest

class TataMapDemoUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
    func testSubmitValue() {
            let app = XCUIApplication()
            app.launch()

            let expectation = "Taxi"
            app.btnTaxi.tap()
//            if(app.lblTaxi.staticTexts["Taxi"].exists){
//                XCTAssertTrue(app.staticTexts[expectation].exists)
//            }else{
//                XCTAssertFalse(false, "Lable Empty")
//            }
        
        let cell = app.cells.element(matching: .cell, identifier: "ListOfVehicleCell_0")
        cell.tap()

            app.btnAuto.tap()
            if(app.lblTaxi.staticTexts["Taxi"].exists){
                XCTAssertTrue(app.staticTexts[expectation].exists)
            }else{
                XCTAssertFalse(false, "Lable Empty")
            }
            
        
//            app.btnTaxi.tap()
 //           app.inputField.typeText(expectation)
 //           app.submitButton.tap()

            XCTAssertTrue(app.staticTexts[expectation].exists)
        }
    
    func testScrollTable() {
        let app = XCUIApplication()
        app.launch()
        let table = app.tables.element(boundBy: 0)
        let lastCell = table.cells.element(boundBy: table.cells.count-1)
        table.scrollToElement(element: lastCell)
    }
}


// in your ui test target...
private extension XCUIApplication {
    var btnAuto: XCUIElement { self.buttons["btnAuto"] }
    
    var lblTaxi: XCUIElement { self.staticTexts["lblTaxi"] }
    var btnTaxi: XCUIElement { self.buttons["btnTaxi"] }
    
    var tblVehicles: XCUIElement { self.tables["tblVehicles"] }
    
}


extension XCUIElement {
    func scrollToElement(element: XCUIElement) {
        while !element.visible() {
            swipeUp()
        }
    }

    func visible() -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }
}
