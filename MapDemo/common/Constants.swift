//
//  Constants.swift
//  TataMapDemo
//
//  Created by Kayaan on 03/06/21.
//  Copyright © 2021 Kayaan Parikh. All rights reserved.
//

import Foundation
import UIKit

enum VehicleType:Int,CustomStringConvertible, CaseIterable  {
     case Taxi
     case Pooling
 
     public var description: String {
      get {
        switch self {
            case .Taxi:
                return "TAXI"
            case .Pooling:
                return "POOLING"
        }
      }
    }
}

let BASE_URL = "https://fake-poi-api.mytaxi.com/?p1Lat=18.910000&p1Lon=72.809998&p2Lat=18.5204&p2Lon=73.8567"

let GOOGLE_MAP_STYLE_ID = "e0b02febd183d35f"
let GOOGLE_MAP_API_KEY = "AIzaSyA3sSGY8TZJVl-FGYdwNaP5vBQ4qM-UDy8"
//AIzaSyA3sSGY8TZJVl-FGYdwNaP5vBQ4qM-UDy8
