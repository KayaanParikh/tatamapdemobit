//
//  GoogleMapVC.swift
//  TataMapDemo
//
//  Created by Kayaan on 03/06/21.
//  Copyright © 2021 Kayaan Parikh. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapVC: UIViewController {
    
    let bottomSheetVC : BottomSheetViewController =  BottomSheetViewController()
    @IBOutlet var pinImage: UIImageView!
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    private var mapsViewModel : MapsViewModel!
    var bounds = GMSCoordinateBounds()
    var moveRequired = false
    
    private let locationManager = CLLocationManager()
    var geoCoder :CLGeocoder!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate(){
        self.mapView.delegate = self
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        geoCoder = CLGeocoder()
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 130, right: 0)
        styleGoogleMap()
       
        self.mapsViewModel =  MapsViewModel()
        self.mapsViewModel.bindMapsViewModelToController = {
            DispatchQueue.main.async {
                if self.mapsViewModel.mapsData.poiList[0].fleetType?.count ?? 0 > 0 {
                    self.showCurrentLocationOnMap(poiList: self.mapsViewModel.mapsData.poiList )
                self.addBottomSheetView()
                }
            }
            
        }
    }
    
    func addBottomSheetView() {
 
        if self.children.count > 0{
             let viewControllers:[UIViewController] = self.children
                viewControllers.last?.willMove(toParent: nil)
                viewControllers.last?.removeFromParent()
                viewControllers.last?.view.removeFromSuperview()
         }
        bottomSheetVC.fromMainView = true
        bottomSheetVC.mapsViewModel = self.mapsViewModel
//        bottomSheetVC.dataSourceVehicle = self.mapsViewModel.mapsData.poiList ?? []
        bottomSheetVC.view.makeCorner(withRadius: 20.0)
        self.addChild(bottomSheetVC)
        self.view.addSubview(bottomSheetVC.view)
        bottomSheetVC.didMove(toParent: self)
   
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showCurrentLocationOnMap(poiList:[PoiList]){

        mapView.clear() // clear the map
        
        for data in poiList{
            
            let location = CLLocationCoordinate2D(latitude: Double(data.coordinate?.latitude ?? 0.0).rounded(toPlaces: 5), longitude: Double(data.coordinate?.longitude ?? 0.0).rounded(toPlaces: 5))
//            print("location: \(location)")
            
            let marker = GMSMarker()
            
            marker.position = location
            if(data.fleetType?.uppercased() == VehicleType.Taxi.description){
                marker.icon = #imageLiteral(resourceName: "cabNew")
            }else if(data.fleetType?.uppercased() == VehicleType.Pooling.description){
                marker.icon = #imageLiteral(resourceName: "carPool")
            }
            marker.snippet = data.fleetType
            marker.map = mapView
            bounds = bounds.includingCoordinate(marker.position)

        }
        if(moveRequired == false){
            let update = GMSCameraUpdate.fit(bounds, withPadding: 20)
            self.mapView.animate(with: update)
        }
    }
}

extension GoogleMapVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
    }
    
}


// MARK: - GMSMapViewDelegate
extension GoogleMapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      
        let lat = position.target.latitude
        let lng = position.target.longitude
        
        // Create Location
        let location = CLLocation(latitude: lat, longitude: lng)
        
        // Geocode Location
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks{
                let pm = placemarks as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks[0]
                   
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print(addressString)
                    self.commonInforMessage(successTitle: "Location :", body: addressString)
              }
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
       // addressLabel.lock()
        
        if (gesture) {
            self.pinImage.isHidden = false
       //     mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
            moveRequired = true
            self.callToViewModelForUIUpdate()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.pinImage.isHidden = true
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 14.0)
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("Drag started!")

    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("ON Drag marker!")

    }
    
    func mapView (_ mapView: GMSMapView, didEndDragging didEndDraggingMarker: GMSMarker) {

        print("Drag ended!")
        print("Update Marker data if stored somewhere.")

    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
       // mapCenterPinImage.fadeIn(0.25)
        mapView.animate(toLocation: CLLocationCoordinate2DMake(Double(self.mapsViewModel.mapsData.poiList[0].coordinate?.latitude ?? 0.0).rounded(toPlaces: 5), Double(self.mapsViewModel.mapsData.poiList[0].coordinate?.longitude ?? 0.0).rounded(toPlaces: 5)))
//        mapView.camera = camera
        mapView.selectedMarker = nil
        
        return false
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
    }
    
    private func styleGoogleMap(){
        do {
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                    mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)

                } else {
                    NSLog("Unable to find style.json")
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
    }
}
