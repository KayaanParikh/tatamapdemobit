
import UIKit

import LocalAuthentication
import CoreLocation

class BottomSheetViewController: UIViewController {
    
    private var dataSource : VehicleTableViewDataSource<ListOfVehicleCell,PoiList>!

    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var svCar: UIStackView!
    
    @IBOutlet weak var btnTaxi: UIButton!
    @IBOutlet weak var btnPooling: UIButton!
    @IBOutlet weak var btnAuto: UIButton!
    
    @IBOutlet weak var lblTaxi: UILabel!
    @IBOutlet weak var lblPool: UILabel!
    @IBOutlet weak var lblAuto: UILabel!
    
    var mapsViewModel : MapsViewModel!
    
    @IBOutlet weak var tblVehicles: UITableView!
    
    var fromMainView = false
    var isBottomSheetOpen = false
    
    var fullView : CGFloat = 0.0
    
    var partialView: CGFloat = 0.0
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.initialSetup()
        
        btnAuto.accessibilityIdentifier = "btnAuto"
        btnTaxi.accessibilityIdentifier = "btnTaxi"
        lblTaxi.accessibilityIdentifier = "lblTaxi"
    
        tblVehicles.accessibilityIdentifier = "ListOfVehicleCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblVehicles.register(UINib(nibName: "ListOfVehicleCell", bundle: nil), forCellReuseIdentifier: "ListOfVehicleCell")
        self.tblVehicles.rowHeight = UITableView.automaticDimension
        self.tblVehicles.estimatedRowHeight = 100
        
        svCar.addBottomBorderWithColor(color: #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1), width: 0.8)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageArrowTapped))
        self.imgArrow.isUserInteractionEnabled = true
        self.imgArrow.addGestureRecognizer(tapGestureRecognizer)
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        view.addGestureRecognizer(gesture)
        self.tblVehicles.backgroundColor = .white
        self.btnTaxiPressed()
        self.tblVehicles.reloadData()
    }
    
    func updateDataSource(typeOfVehicle:VehicleType = .Taxi,poiList:[PoiList] = []){
        
        self.dataSource = VehicleTableViewDataSource(cellIdentifier: "ListOfVehicleCell", items: poiList, configureCell: { (cell, evm) in
           
                   
            if("\(evm.fleetType?.uppercased() ?? "")" == VehicleType.Taxi.description){
                       cell.imgVehicle?.image = #imageLiteral(resourceName: "cabNew")
                   }else if ("\(evm.fleetType ?? "")" == VehicleType.Pooling.description){
                       cell.imgVehicle?.image = #imageLiteral(resourceName: "carPool")
                   }else{
                       cell.imgVehicle?.image = #imageLiteral(resourceName: "rickshaw")
                   }
                   
                   var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
                   let lat: Double = Double("\(evm.coordinate?.latitude ?? 0.0)")!
                   
                   let lon: Double = Double("\(evm.coordinate?.longitude ?? 0.0)")!
                   
                   let ceo: CLGeocoder = CLGeocoder()
                   center.latitude = lat
                   center.longitude = lon

                   let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)

                       var addressString = String()
                   ceo.reverseGeocodeLocation(loc, completionHandler:{   (placemarks, error) in
                           if (error != nil)
                           {
                               print("reverse geodcode fail: \(error!.localizedDescription)")
                           }
                       if placemarks  != nil {
                           let pm = placemarks! as [CLPlacemark]

                               if pm.count > 0 {
                                   let pm = placemarks![0]
                                   if pm.subLocality != nil {
                                       addressString = addressString + pm.subLocality! + ", "
                                   }
                                   if pm.thoroughfare != nil {
                                       addressString = addressString + pm.thoroughfare! + ", "
                                   }
                                   if pm.locality != nil {
                                       addressString = addressString + pm.locality! + ", "
                                   }
                                   if pm.country != nil {
                                       addressString = addressString + pm.country! + ", "
                                   }
                                   if pm.postalCode != nil {
                                       addressString = addressString + pm.postalCode! + " "
                                   }
                                   cell.lblPlace.text = "\(addressString)"
                             }
                       }
                           
                   })
                   
                    cell.innerView.setCardView()
            
           })
           
           DispatchQueue.main.async {
               self.tblVehicles.dataSource = self.dataSource
               self.tblVehicles.reloadData()
           }
       }
        
    @IBAction func btnTaxiPressed() {
        self.updateDataSource(typeOfVehicle: .Taxi, poiList: mapsViewModel.mapsData.poiList.filter({$0.fleetType == VehicleType.Taxi.description}).map({$0}))
        self.btnTaxi.isSelected = true
        self.btnPooling.isSelected = false
        self.btnAuto.isSelected = false
        self.lblTaxi.textColor = .black
        self.lblPool.textColor = .lightGray
        self.lblAuto.textColor = .lightGray
        self.tblVehicles.reloadData()
    }
    
    @IBAction func btnPoolPressed() {
        self.updateDataSource(typeOfVehicle: .Pooling, poiList: mapsViewModel.mapsData.poiList.filter({$0.fleetType == VehicleType.Pooling.description}).map({$0}))
        self.btnTaxi.isSelected = false
        self.btnPooling.isSelected = true
        self.btnAuto.isSelected = false
        self.lblTaxi.textColor = .lightGray
        self.lblPool.textColor = .black
        self.lblAuto.textColor = .lightGray
        self.tblVehicles.reloadData()
    }
    @IBAction func btnAutoPressed() {
        self.updateDataSource()
//        objUpdate.removeAll()
        self.btnTaxi.isSelected = false
        self.btnPooling.isSelected = false
        self.btnAuto.isSelected = true
        self.lblTaxi.textColor = .lightGray
        self.lblPool.textColor = .lightGray
        self.lblAuto.textColor = .black
        self.tblVehicles.reloadData()
    }
    
    @objc func imageArrowTapped(){
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            
            let frame = self?.view.frame
             if (self?.isBottomSheetOpen == false){
                self?.isBottomSheetOpen = true
                let yComponent = self?.fullView
                self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
                self?.imgArrow.transform = CGAffineTransform(rotationAngle: CGFloat(CGFloat(Double.pi)))
                
            }else{
                self?.isBottomSheetOpen = false
                let yComponent = self?.partialView
               self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
                self!.imgArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi) - 3.14159)
            }
        })
        
    }
    
    func initialSetup()
    {
        fullView = 320
        partialView = self.view.frame.size.height * 0.80
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
         let frame = self?.view.frame
         let yComponent = self?.partialView
         self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func close() {
        
        DispatchQueue.main.async {
        let frame = self.view.frame
       UIView.animate(withDuration: 0.6, animations: { [weak self] in
            self?.isBottomSheetOpen = false
            let yComponent = self?.partialView
        
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame.width, height: frame.height)
            self?.imgArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi) - 3.14159)
        })
        }
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        let y = self.view.frame.minY
        if ( y + translation.y >= fullView) && (y + translation.y <= partialView ) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                    
                    self.imgArrow.transform = CGAffineTransform(rotationAngle: CGFloat( CGFloat(Double.pi) - 3.14159));
                   // self.imgArrow.image = #imageLiteral(resourceName: "pullArrowUp")
                  
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                    self.imgArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                    //self.imgArrow.image = #imageLiteral(resourceName: "pullArrowDown")
                }
                
                }, completion: nil)
        }
    }
    
    
    
}
