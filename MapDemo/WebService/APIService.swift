//
//  APIService.swift
//  TataMapDemo
//
//  Created by Kayaan Parikh on 03/06/20.
//  Copyright © 2021 Kayaan Parikh. All rights reserved.
//

import Foundation


class APIService :  NSObject {
    
    private let sourcesURL = URL(string: BASE_URL)!
    
    func apiToGetMapsData(completion : @escaping (MapModelData) -> ()){
        
        URLSession.shared.dataTask(with: sourcesURL) { (data, urlResponse, error) in
            if let data = data {

                let jsonDecoder = JSONDecoder()

                let empData = try! jsonDecoder.decode(MapModelData.self, from: data)

                    completion(empData)
            }

        }.resume()
    }
    
}


extension NSURLRequest {
    static func allowsAnyHTTPSCertificateForHost(host: String) -> Bool {
        return true
    }
}
